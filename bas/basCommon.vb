﻿Imports System.Text
Imports System.IO
Imports Npgsql
Imports System.Data.SqlClient

Module basCommon
    Public conn As New NpgsqlConnection
    Public objConnect_LocalDB As New SqlConnection      'SQL Connection

    ''' <summary>
    ''' subOutLog
    ''' </summary>
    ''' <param name="Proc">0・・・Inf、2・・・War、-9・・・Err</param>
    ''' <param name="msg">出力メッセージ</param>
    Public Sub subOutLog(ByVal Proc As Integer, ByVal msg As String, ByVal strMethod As String)

        Dim strOutMsg As String = vbNullString
        Dim strPath As String = My.Settings.LogPath
        Dim strYYYY As String = Now.Year.ToString
        Dim strMM As String = Now.Month.ToString("0#")
        Dim strDD As String = Now.Day.ToString("0#")
        Dim strDateTime As String = Now.ToString("yyyy/MM/dd HH:mm:ss")

        If strPath.EndsWith("\") = False Then
            strPath += "\"
        End If

        '---ログ出力用のフォルダ(yyyy)がなければ作成する。
        If System.IO.Directory.Exists(strPath & strYYYY & "\") = False Then
            FileSystem.MkDir(strPath & strYYYY)
        End If

        '---ログ出力用のフォルダ(mm)がなければ作成する。
        If System.IO.Directory.Exists(strPath & strYYYY & "\" & strMM) = False Then
            FileSystem.MkDir(strPath & strYYYY & "\" & strMM)
        End If

        Select Case Proc
            Case RET_NORMAL
                strOutMsg = "[INF](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
            Case RET_WARNING
                strOutMsg = "[WAR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
            Case RET_ERROR
                strOutMsg = "[ERR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
        End Select

        '---ログファイルのフルパス生成
        strPath = strPath & "\" & strYYYY & "\" & strMM & "\" & strYYYY & strMM & strDD & ".log"

        '---ログファイルに出力
        Dim writer As StreamWriter = New StreamWriter(strPath, True, Encoding.GetEncoding("shift_jis"))
        writer.WriteLine(strOutMsg)
        writer.Close()
    End Sub

    ''' <summary>
    ''' ログをコンソールとファイル両方に出力する
    ''' </summary>
    ''' <param name="Proc">0=正常、-2=ワーニング、-9=エラー</param>
    ''' <param name="msg">出力メッセージ</param>
    ''' <param name="strMethod">メソッド名</param>
    Public Sub subOutMessage(ByVal Proc As Integer, ByVal msg As String, ByVal strMethod As String)
        Dim strDateTime As String = Now.ToString("yyyy/MM/dd HH:mm:ss")
        Dim strOutMsg As String = vbNullString
        Dim objStyle As New MsgBoxStyle
        Dim strMsgBox As String = vbNullString

        Select Case Proc
            Case RET_NORMAL
                strOutMsg = "[INF](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
                objStyle = MsgBoxStyle.Information
            Case RET_WARNING
                strOutMsg = "[WAR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
                objStyle = MsgBoxStyle.Exclamation
            Case RET_ERROR
                strOutMsg = "[ERR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
                objStyle = MsgBoxStyle.Critical
        End Select

        '---コンソールに出力
        Console.WriteLine(strOutMsg)

        '---メッセージボックスの表示
        MsgBox(msg, objStyle, "メッセージ出力")

        Call subOutLog(Proc, msg, strMethod)
    End Sub

#Region "SQLサーバConnection"
    Public Function fncDBConnect() As Boolean
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try

            'ﾛｸﾞ用ﾃｰﾌﾞﾙ接続
            Dim connectionString3 As New System.Text.StringBuilder
            connectionString3.Append("Data Source=")
            connectionString3.Append(My.Settings.DB_Server)
            connectionString3.Append(";")
            connectionString3.Append("Initial Catalog=")
            connectionString3.Append(My.Settings.DB_Name)
            connectionString3.Append(";")
            connectionString3.Append("UID=")
            connectionString3.Append(My.Settings.DB_User)
            connectionString3.Append(";")
            connectionString3.Append("PWD=")
            connectionString3.Append(My.Settings.DB_Pass)
            connectionString3.Append(";")
            connectionString3.Append("Integrated Security=false;")
            connectionString3.Append("Connect Timeout = 0;")

            objConnect_LocalDB = New SqlConnection
            With objConnect_LocalDB
                .ConnectionString = connectionString3.ToString
                .Open()
            End With

            Return True

        Catch ex As Exception

            Call subOutLog(RET_ERROR, "データベース接続 失敗 (エラー内容 = " & ex.Message & ") ", strMethod)

            Return False

        End Try

    End Function
#End Region

#Region "SQLサーバDisConnect"
    ''' <summary>
    ''' SQLサーバDisConnect
    ''' </summary>
    ''' <returns></returns>
    Public Function fncDBDisConnect() As Boolean
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try

            If Not objConnect_LocalDB Is Nothing Then
                objConnect_LocalDB.Close()
                objConnect_LocalDB.Dispose()
            End If

            Return True
        Catch ex As Exception
            Call subOutLog(RET_ERROR, "DisConnect 失敗 (エラー内容 = " & ex.Message & ") ", strMethod)

            Return False
        End Try
    End Function
#End Region

#Region "SQL発行"
    ''' <summary>
    ''' SQL発行
    ''' </summary>
    ''' <param name="objCon"></param>
    ''' <param name="strSQL"></param>
    ''' <returns></returns>
    Public Function fncAdptSQL(ByRef objCon As SqlConnection, ByVal strSQL As String) As DataSet
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        fncAdptSQL = New DataSet

        Try
            Dim objAdpt As SqlDataAdapter = New SqlDataAdapter(strSQL, objCon)

            objAdpt.SelectCommand.CommandTimeout = 0
            objAdpt.Fill(fncAdptSQL)

            objAdpt.Dispose()
            fncAdptSQL.Dispose()

        Catch ex As Exception

            Call subOutLog(RET_ERROR, "データベース接続 失敗 (エラー内容 = " & ex.Message & ") ", strMethod)

            Return Nothing
        End Try

    End Function

    ''' <summary>
    ''' DB登録
    ''' </summary>
    ''' <param name="objCon"></param>
    ''' <param name="strSQL"></param>
    ''' <returns></returns>
    Public Function fncExecuteNonQuery(ByRef objCon As SqlConnection, ByVal strSQL As String) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try

            Dim objCommand As SqlCommand

            objCommand = objCon.CreateCommand()
            objCommand.CommandText = strSQL

            Dim iResult As Integer = objCommand.ExecuteNonQuery()
            objCommand.Dispose()

            Return iResult

        Catch ex As Exception

            Call subOutLog(RET_ERROR, "データベース登録 失敗 (エラー内容 = " & ex.Message & ") ", strMethod)

            Return RET_ERROR
        End Try

    End Function

#End Region
End Module
