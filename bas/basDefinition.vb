﻿Module basDefinition
#Region "定数定義"

    Public Const RET_NORMAL As Integer = 0
    Public Const RET_NOTFOUND As Integer = -1
    Public Const RET_WARNING As Integer = -2
    Public Const RET_ERROR As Integer = -9
#End Region

    Public typMiFare As New TYPE_MIFARE
    Public aryFoodUser() As TYPE_FoodUser

#Region "MIFAREカードデータ用退避エリア"
    Public Structure TYPE_MIFARE
        Public CID As String
        Public TID As String
        Public READ_DATE As String
        Public READ_TIME As String
        Public MENU_ANO As String
    End Structure
#End Region

    Public Structure TYPE_FoodUser
        Public UserID As String
    End Structure

End Module
