﻿Public Class frmAggregate

#Region "FormEvents"

    ''' <summary>
    ''' FormLoad
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub frmAggregate_Load(sender As Object, e As EventArgs) Handles Me.Load
        '初期化
        Call subInit()
    End Sub

    ''' <summary>
    ''' FormClosing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub frmAggregate_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        frmMenu.Visible = True
    End Sub

#End Region

#Region "ControlEvents"

    ''' <summary>
    ''' 検索ボタン押下時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        If Me.optDIV1.Checked = True Then
            If Me.optDayMon1.Checked = True Then
                Call subGetHistoryDataDate()
            Else
                Call subGetHistoryData()
            End If
        Else
            If Me.optDayMon1.Checked = True Then
                Call subGetAggregateMonthDate()
            Else
                Call subGetAggregateMonth()
            End If
        End If

        Me.lblRowCount.Text = Me.dgvList.Rows.Count
    End Sub

    ''' <summary>
    ''' クリアボタン押下時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        Call subInit()
        Call subDivide()
    End Sub

    ''' <summary>
    ''' 閉じるボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
        Me.Close()

        frmMenu.Visible = True
    End Sub

    ''' <summary>
    ''' 職員別選択時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub optDIV1_CheckedChanged(sender As Object, e As EventArgs) Handles optDIV1.CheckedChanged
        Call subDivide()
    End Sub

    ''' <summary>
    ''' メニュー別集計選択時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub optDIV2_CheckedChanged(sender As Object, e As EventArgs) Handles optDIV2.CheckedChanged
        Call subDivide()
    End Sub

    ''' <summary>
    ''' 月個別選択時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub optPurchase1_CheckedChanged(sender As Object, e As EventArgs) Handles optPurchase1.CheckedChanged
        If optPurchase1.Checked = True Then
            Call subDivide()

            Me.nupYear.Enabled = True
            Me.nupMont.Enabled = True

            Me.nupYearFrom.Enabled = False
            Me.nupMontFrom.Enabled = False
            Me.nupYearTo.Enabled = False
            Me.nupMontTo.Enabled = False

        Else
            Me.nupYear.Enabled = False
            Me.nupMont.Enabled = False

            Me.nupYearFrom.Enabled = True
            Me.nupMontFrom.Enabled = True
            Me.nupYearTo.Enabled = True
            Me.nupMontTo.Enabled = True

        End If
    End Sub

    ''' <summary>
    ''' CSV出力押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdSaveCSV_Click(sender As Object, e As EventArgs) Handles cmdSaveCSV.Click
        With sfdCsvFile
            Dim strFileName As String

            If optPurchase1.Checked = True Then
                strFileName = Replace(My.Settings.SAVE_CSV_SUM, "%yyyy", nupYear.Value.ToString)
                strFileName = Replace(strFileName, "%mm", nupMont.Value.ToString)
            Else
                strFileName = Replace(My.Settings.SAVE_CSV_SUM_FT, "%yyyy1", nupYearFrom.Value.ToString)
                strFileName = Replace(strFileName, "%mm1", nupMontFrom.Value.ToString)
                strFileName = Replace(strFileName, "%yyyy2", nupYearTo.Value.ToString)
                strFileName = Replace(strFileName, "%mm2", nupMontTo.Value.ToString)
            End If

            .DefaultExt = ".csv"
            .FileName = strFileName
            If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                Call SaveToCsv()
                Call MsgBox("「" & .FileName & "」を出力しました。", MsgBoxStyle.Information, "CSV出力")
            End If
        End With
    End Sub

    ''' <summary>
    ''' 月別のチェック状態が変わったとき
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub optDayMon2_CheckedChanged(sender As Object, e As EventArgs) Handles optDayMon2.CheckedChanged
        If Me.optDayMon2.Checked = True Then
            Call subDivide()
        End If
    End Sub

    ''' <summary>
    ''' 日別のチェック状態が変わったとき
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub optDayMon1_CheckedChanged(sender As Object, e As EventArgs) Handles optDayMon1.CheckedChanged
        If Me.optDayMon1.Checked = True Then
            Call subDivide()
        End If

    End Sub

    ''' <summary>
    ''' 月個別のチェック状態が変わったとき
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub optPurchase2_CheckedChanged(sender As Object, e As EventArgs) Handles optPurchase2.CheckedChanged
        If optPurchase2.Checked = True Then
            Call subDivide()
        End If
    End Sub

#End Region

#Region "SubRoutine"
    ''' <summary>
    ''' 職員単位での集計
    ''' </summary>
    Private Sub subGetHistoryData()
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim Proc As Integer = 0
        Dim ds As New DataSet
        Dim intCounter As Integer = 0
        Dim strWork As String = vbNullString
        Dim strWork1 As String = vbNullString
        Dim strWork2 As String = vbNullString

        Try
            Call fncDBConnect()

            dgvList.Rows.Clear()
            If Me.optPurchase1.Checked = True Then
                strWork1 = Me.nupYear.Value & Me.nupMont.Value.ToString("0#")
            Else
                strWork1 = Me.nupYearFrom.Value & Me.nupMontFrom.Value.ToString("0#")
                strWork2 = Me.nupYearTo.Value & Me.nupMontTo.Value.ToString("0#")
            End If
            strSQL.Clear()
            strSQL.AppendLine("SELECT DISTINCT")
            strSQL.AppendLine("buyCID,")
            strSQL.AppendLine("buyUserEmpCode,")
            strSQL.AppendLine("buyGroupCode,")
            strSQL.AppendLine("buySectionCode,")
            strSQL.AppendLine("buyUserName,")
            strSQL.AppendLine("buyUserKana,")
            strSQL.AppendLine("YearMonth,")
            strSQL.AppendLine("CountA,")
            strSQL.AppendLine("CountB,")
            'strSQL.AppendLine("CountC,")
            strSQL.AppendLine("Amount")                 'ADD By Watanabe 2020.10.26
            strSQL.AppendLine("FROM vwAggregate")

            If Me.optPurchase1.Checked = True Then
                'strSQL.AppendLine("WHERE (YearMonth = '" & strWork1 & "') OR (YearMonthA = '" & strWork1 & "') OR (YearMonthB = '" & strWork1 & "') OR (YearMonthC = '" & strWork1 & "')")
                strSQL.AppendLine("WHERE (YearMonth = '" & strWork1 & "') OR (YearMonthA = '" & strWork1 & "') OR (YearMonthB = '" & strWork1 & "')")
            Else
                'strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "') OR (YearMonthA >= '" & strWork1 & "' AND YearMonthA <= '" & strWork2 & "') OR (YearMonthB >= '" & strWork1 & "' AND YearMonthB <= '" & strWork2 & "') OR (YearMonthC >= '" & strWork1 & "' AND YearMonthC <= '" & strWork2 & "')")
                strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "') OR (YearMonthA >= '" & strWork1 & "' AND YearMonthA <= '" & strWork2 & "') OR (YearMonthB >= '" & strWork1 & "' AND YearMonthB <= '" & strWork2 & "')")
            End If

            If Me.txtEmpCode.Text.Trim <> "" Then
                strSQL.AppendLine("AND buyUserEmpCode LIKE '%" & Me.txtEmpCode.Text.Trim & "'")
            End If
            If Me.txtKanaName.Text.Trim <> "" Then
                strSQL.AppendLine("AND buyUserKana LIKE '" & Me.txtKanaName.Text.Trim & "%'")
            End If

            strSQL.AppendLine("ORDER BY YearMonth")

            ds = fncAdptSQL(objConnect_LocalDB, strSQL.ToString)

            If ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                Call MsgBox("検索条件に合うデータが存在しませんでした。" & vbCrLf & "検索条件を変更して再度検索して下さい。", MsgBoxStyle.Exclamation, "所見購入履歴検索")
                Exit Sub
            End If

            For intCounter = 0 To ds.Tables(0).Rows.Count - 1
                dgvList.Rows.Add()

                With dgvList.Rows(intCounter)
                    'strWork = Me.nupYear.Value & "/" & Me.nupMont.Value.ToString("0#")
                    strWork = ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(0, 4) & "年"
                    strWork &= ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(4, 2) & "月"
                    .Cells("COL_PURCHASE_DATE").Value = strWork
                    .Cells("COL_GROUP_CD").Value = ds.Tables(0).Rows(intCounter).Item("buyGroupCode")
                    .Cells("COL_SECTION_CD").Value = ds.Tables(0).Rows(intCounter).Item("buySectionCode")
                    .Cells("COL_USER_ID").Value = ds.Tables(0).Rows(intCounter).Item("buyUserEmpCode")
                    .Cells("COL_USER_NAME").Value = ds.Tables(0).Rows(intCounter).Item("buyUserName")
                    .Cells("COL_USER_KANA").Value = ds.Tables(0).Rows(intCounter).Item("buyUserKana")
                    .Cells("COL_MENU_A").Value = ds.Tables(0).Rows(intCounter).Item("CountA")
                    .Cells("COL_MENU_B").Value = ds.Tables(0).Rows(intCounter).Item("CountB")
                    '.Cells("COL_MENU_C").Value = ds.Tables(0).Rows(intCounter).Item("CountC")
                    '.Cells("COL_ID").Value = ds.Tables(0).Rows(intCounter).Item("buyUserID")
                    .Cells("COL_CID").Value = ds.Tables(0).Rows(intCounter).Item("buyCID")
                    .Cells("COL_SUM").Value = ds.Tables(0).Rows(intCounter).Item("Amount") 'ADD By Watanabe 2020.10.26
                End With
            Next intCounter

        Catch ex As Exception
            Call subOutMessage(RET_ERROR, ex.Message, strMethod)
        Finally
            Call fncDBDisConnect()
        End Try
    End Sub

    ''' <summary>
    ''' 職員単位での集計 ※日別表示
    ''' </summary>
    Private Sub subGetHistoryDataDate()
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim Proc As Integer = 0
        Dim ds As New DataSet
        Dim intCounter As Integer = 0
        Dim strWork As String = vbNullString
        Dim strWork1 As String = vbNullString
        Dim strWork2 As String = vbNullString
        Dim objUserProfile As New d_prim_manurtyDataSetTableAdapters.UserProfilesTableAdapter
        Dim dt As New d_prim_manurtyDataSet.UserProfilesDataTable
        Dim strEmpCode As String = vbNullString


        Try
            Call fncDBConnect()

            dgvList.Rows.Clear()
            If Me.optPurchase1.Checked = True Then
                strWork1 = Me.nupYear.Value & Me.nupMont.Value.ToString("0#")
            Else
                strWork1 = Me.nupYearFrom.Value & Me.nupMontFrom.Value.ToString("0#") & "01"
                strWork2 = Me.nupYearTo.Value & Me.nupMontTo.Value.ToString("0#") & "31"
            End If
            strSQL.Clear()
            strSQL.AppendLine("SELECT DISTINCT")
            strSQL.AppendLine("buyCID,")
            strSQL.AppendLine("buyGroupCode,")
            strSQL.AppendLine("buySectionCode,")
            strSQL.AppendLine("buyUserEmpCode,")
            strSQL.AppendLine("buyUserName,")
            strSQL.AppendLine("buyUserKana,")
            strSQL.AppendLine("YearMonth,")
            strSQL.AppendLine("CountA,")
            strSQL.AppendLine("CountB,")
            'strSQL.AppendLine("CountC,")
            strSQL.AppendLine("Amount")             'ADD By Watanabe 2020.10.26
            strSQL.AppendLine("FROM vwAggregateDate")

            If Me.optPurchase1.Checked = True Then
                'strSQL.AppendLine("WHERE (SUBSTRING(YearMonth,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthA,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthB,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthC,0,7) = '" & strWork1 & "')")
                strSQL.AppendLine("WHERE (SUBSTRING(YearMonth,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthA,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthB,0,7) = '" & strWork1 & "')")
            Else
                'strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "') OR (YearMonthA >= '" & strWork1 & "' AND YearMonthA <= '" & strWork2 & "') OR (YearMonthB >= '" & strWork1 & "' AND YearMonthB <= '" & strWork2 & "') OR (YearMonthC >= '" & strWork1 & "' AND YearMonthC <= '" & strWork2 & "')")
                strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "') OR (YearMonthA >= '" & strWork1 & "' AND YearMonthA <= '" & strWork2 & "') OR (YearMonthB >= '" & strWork1 & "' AND YearMonthB <= '" & strWork2 & "')")
            End If

            If Me.txtEmpCode.Text.Trim <> "" Then
                strSQL.AppendLine("AND buyUserEmpCode LIKE '%" & Me.txtEmpCode.Text.Trim & "'")
            End If
            If Me.txtKanaName.Text.Trim <> "" Then
                strSQL.AppendLine("AND buyUserKana LIKE '" & Me.txtKanaName.Text.Trim & "%'")
            End If

            strSQL.AppendLine("ORDER BY YearMonth")

            ds = fncAdptSQL(objConnect_LocalDB, strSQL.ToString)

            If ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                Call MsgBox("検索条件に合うデータが存在しませんでした。" & vbCrLf & "検索条件を変更して再度検索して下さい。", MsgBoxStyle.Exclamation, "所見購入履歴検索")
                Exit Sub
            End If

            For intCounter = 0 To ds.Tables(0).Rows.Count - 1
                dgvList.Rows.Add()

                With dgvList.Rows(intCounter)
                    'strWork = Me.nupYear.Value & "/" & Me.nupMont.Value.ToString("0#")
                    strWork = ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(0, 4) & "年"
                    strWork &= ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(4, 2) & "月"
                    strWork &= ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(6, 2) & "日"

                    strEmpCode = ds.Tables(0).Rows(intCounter).Item("buyUserEmpCode")
                    objUserProfile.FillByEmpCode(dt, strEmpCode)

                    If dt.Rows.Count > 0 Then
                        .Cells("COL_GROUP_CD").Value = dt.Rows(0).Item("groupcode")
                        .Cells("COL_SECTION_CD").Value = dt.Rows(0).Item("sectioncode")
                    Else
                        .Cells("COL_GROUP_CD").Value = ""
                        .Cells("COL_SECTION_CD").Value = ""
                    End If

                    .Cells("COL_PURCHASE_DATE").Value = strWork
                    .Cells("COL_USER_ID").Value = ds.Tables(0).Rows(intCounter).Item("buyUserEmpCode")
                    .Cells("COL_USER_NAME").Value = ds.Tables(0).Rows(intCounter).Item("buyUserName")
                    .Cells("COL_USER_KANA").Value = ds.Tables(0).Rows(intCounter).Item("buyUserKana")
                    .Cells("COL_MENU_A").Value = ds.Tables(0).Rows(intCounter).Item("CountA")
                    .Cells("COL_MENU_B").Value = ds.Tables(0).Rows(intCounter).Item("CountB")
                    '.Cells("COL_MENU_C").Value = ds.Tables(0).Rows(intCounter).Item("CountC")
                    '.Cells("COL_ID").Value = ds.Tables(0).Rows(intCounter).Item("buyUserID")
                    .Cells("COL_CID").Value = ds.Tables(0).Rows(intCounter).Item("buyCID")
                    .Cells("COL_SUM").Value = ds.Tables(0).Rows(intCounter).Item("Amount")      'ADD By Watanabe 2020.10.26
                End With
            Next intCounter

        Catch ex As Exception
            Call subOutMessage(RET_ERROR, ex.Message, strMethod)
        Finally
            Call fncDBDisConnect()
        End Try
    End Sub

    ''' <summary>
    ''' メニューごとの件数
    ''' </summary>
    Private Sub subGetAggregateMonth()
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim Proc As Integer = 0
        Dim ds As New DataSet
        Dim intCounter As Integer = 0
        Dim strWork As String = vbNullString
        Dim strWork1 As String = vbNullString
        Dim strWork2 As String = vbNullString

        Try
            Call fncDBConnect()

            dgvList.Rows.Clear()

            If Me.optPurchase1.Checked = True Then
                strWork1 = Me.nupYear.Value & Me.nupMont.Value.ToString("0#")
            Else
                strWork1 = Me.nupYearFrom.Value & Me.nupMontFrom.Value.ToString("0#")
                strWork2 = Me.nupYearTo.Value & Me.nupMontTo.Value.ToString("0#")
            End If
            strSQL.Clear()
            'strSQL.AppendLine("SELECT DISTINCT")
            'strSQL.AppendLine("YearMonth,")
            'strSQL.AppendLine("CountA,")
            'strSQL.AppendLine("CountB,")
            'strSQL.AppendLine("CountC,")
            'strSQL.AppendLine("Amount")         'ADD By Watanabe 2020.10.26
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("YearMonth,")
            strSQL.AppendLine("SUM(CountA) AS CountA,")
            strSQL.AppendLine("SUM(CountB) AS CountB,")
            'strSQL.AppendLine("SUM(CountC) AS CountC,")
            strSQL.AppendLine("SUM(Amount) AS Amount")         'ADD By Watanabe 2020.10.26
            strSQL.AppendLine("FROM vwAggregate")
            'strSQL.AppendLine("WHERE (YearMonth = '" & strWork & "') OR (YearMonthA = '" & strWork & "') OR (YearMonthB = '" & strWork & "') AND (YearMonthC = '" & strWork & "')")

            'If Me.optPurchase1.Checked = True Then
            If Me.optPurchase1.Checked = True Then
                'strSQL.AppendLine("WHERE (YearMonth = '" & strWork1 & "') OR (YearMonthA = '" & strWork1 & "') OR (YearMonthB = '" & strWork1 & "') OR (YearMonthC = '" & strWork1 & "')")
                'strSQL.AppendLine("WHERE (YearMonth = '" & strWork1 & "') OR (YearMonthA = '" & strWork1 & "') OR (YearMonthB = '" & strWork1 & "')")
                strSQL.AppendLine("WHERE (YearMonth = '" & strWork1 & "')")
            Else
                'strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "') OR (YearMonthA >= '" & strWork1 & "' AND YearMonthA <= '" & strWork2 & "') OR (YearMonthB >= '" & strWork1 & "' AND YearMonthB <= '" & strWork2 & "') OR (YearMonthC >= '" & strWork1 & "' AND YearMonthC <= '" & strWork2 & "')")
                'strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "') OR (YearMonthA >= '" & strWork1 & "' AND YearMonthA <= '" & strWork2 & "') OR (YearMonthB >= '" & strWork1 & "' AND YearMonthB <= '" & strWork2 & "')")
                strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "')")
            End If

            strSQL.AppendLine("GROUP BY YearMonth")
            strSQL.AppendLine("ORDER BY YearMonth")

            ds = fncAdptSQL(objConnect_LocalDB, strSQL.ToString)

            If ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                Call MsgBox("検索条件に合うデータが存在しませんでした。" & vbCrLf & "検索条件を変更して再度検索して下さい。", MsgBoxStyle.Exclamation, "所見購入履歴検索")
                Exit Sub
            End If

            For intCounter = 0 To ds.Tables(0).Rows.Count - 1
                strWork = Me.nupYear.Value & "/" & Me.nupMont.Value.ToString("0#")
                dgvList.Rows.Add()

                With dgvList.Rows(intCounter)
                    strWork = ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(0, 4) & "年"
                    strWork &= ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(4, 2) & "月"
                    .Cells("COL_PURCHASE_DATE").Value = strWork
                    .Cells("COL_MENU_A").Value = ds.Tables(0).Rows(intCounter).Item("CountA")
                    .Cells("COL_MENU_B").Value = ds.Tables(0).Rows(intCounter).Item("CountB")
                    '.Cells("COL_MENU_C").Value = ds.Tables(0).Rows(intCounter).Item("CountC")
                    .Cells("COL_SUM").Value = ds.Tables(0).Rows(intCounter).Item("Amount")      'ADD By Watanabe 2020.10.26
                End With
            Next intCounter
        Catch ex As Exception
            Call subOutMessage(RET_ERROR, ex.Message, strMethod)
        Finally
            Call fncDBDisConnect()
        End Try
    End Sub

    ''' <summary>
    ''' メニューごとの件数 ※日別表示
    ''' </summary>
    Private Sub subGetAggregateMonthDate()
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim Proc As Integer = 0
        Dim ds As New DataSet
        Dim intCounter As Integer = 0
        Dim strWork As String = vbNullString
        Dim strWork1 As String = vbNullString
        Dim strWork2 As String = vbNullString

        Try
            Call fncDBConnect()

            dgvList.Rows.Clear()

            If Me.optPurchase1.Checked = True Then
                strWork1 = Me.nupYear.Value & Me.nupMont.Value.ToString("0#")
            Else
                strWork1 = Me.nupYearFrom.Value & Me.nupMontFrom.Value.ToString("0#") & "01"
                strWork2 = Me.nupYearTo.Value & Me.nupMontTo.Value.ToString("0#") & "31"
            End If
            strSQL.Clear()
            'strSQL.AppendLine("SELECT DISTINCT")
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("YearMonth,")
            'strSQL.AppendLine("CountA,")
            'strSQL.AppendLine("CountB,")
            strSQL.AppendLine("SUM(CountA) AS CountA,")
            strSQL.AppendLine("SUM(CountB) AS CountB,")
            'strSQL.AppendLine("CountC,")
            strSQL.AppendLine("SUM(Amount) AS Amount")         'ADD By Watanabe 2020.10.26
            strSQL.AppendLine("FROM vwAggregateDate")
            'strSQL.AppendLine("WHERE (YearMonth = '" & strWork & "') OR (YearMonthA = '" & strWork & "') OR (YearMonthB = '" & strWork & "') AND (YearMonthC = '" & strWork & "')")

            If Me.optPurchase1.Checked = True Then
                'If Me.optDayMon1.Checked = True Then
                'strSQL.AppendLine("WHERE (YearMonth = '" & strWork1 & "') OR (YearMonthA = '" & strWork1 & "') OR (YearMonthB = '" & strWork1 & "') OR (YearMonthC = '" & strWork1 & "')")
                'strSQL.AppendLine("WHERE (SUBSTRING(YearMonth,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthA,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthB,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthC,0,7) = '" & strWork1 & "')")
                'strSQL.AppendLine("WHERE (SUBSTRING(YearMonth,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthA,0,7) = '" & strWork1 & "') OR (SUBSTRING(YearMonthB,0,7) = '" & strWork1 & "')")
                strSQL.AppendLine("WHERE (SUBSTRING(YearMonth,0,7) = '" & strWork1 & "')")
                Else
                    'strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "') OR (YearMonthA >= '" & strWork1 & "' AND YearMonthA <= '" & strWork2 & "') OR (YearMonthB >= '" & strWork1 & "' AND YearMonthB <= '" & strWork2 & "') OR (YearMonthC >= '" & strWork1 & "' AND YearMonthC <= '" & strWork2 & "')")
                    'strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "') OR (YearMonthA >= '" & strWork1 & "' AND YearMonthA <= '" & strWork2 & "') OR (YearMonthB >= '" & strWork1 & "' AND YearMonthB <= '" & strWork2 & "') OR (YearMonthC >= '" & strWork1 & "')")
                    strSQL.AppendLine("WHERE (YearMonth >= '" & strWork1 & "' AND YearMonth <= '" & strWork2 & "')")
            End If

            strSQL.AppendLine("GROUP BY YearMonth")
            strSQL.AppendLine("ORDER BY YearMonth")

            ds = fncAdptSQL(objConnect_LocalDB, strSQL.ToString)

            If ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                Call MsgBox("検索条件に合うデータが存在しませんでした。" & vbCrLf & "検索条件を変更して再度検索して下さい。", MsgBoxStyle.Exclamation, "所見購入履歴検索")
                Exit Sub
            End If

            For intCounter = 0 To ds.Tables(0).Rows.Count - 1
                strWork = Me.nupYear.Value & "/" & Me.nupMont.Value.ToString("0#")
                dgvList.Rows.Add()

                With dgvList.Rows(intCounter)
                    strWork = ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(0, 4) & "年"
                    strWork &= ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(4, 2) & "月"
                    strWork &= ds.Tables(0).Rows(intCounter).Item("YearMonth").ToString.Substring(6, 2) & "日"
                    .Cells("COL_PURCHASE_DATE").Value = strWork
                    .Cells("COL_MENU_A").Value = ds.Tables(0).Rows(intCounter).Item("CountA")
                    .Cells("COL_MENU_B").Value = ds.Tables(0).Rows(intCounter).Item("CountB")
                    '.Cells("COL_MENU_C").Value = ds.Tables(0).Rows(intCounter).Item("CountC")
                    .Cells("COL_SUM").Value = ds.Tables(0).Rows(intCounter).Item("Amount")      'ADD By Watanabe 2020.10.26
                End With
            Next intCounter
        Catch ex As Exception
            Call subOutMessage(RET_ERROR, ex.Message, strMethod)
        Finally
            Call fncDBDisConnect()
        End Try
    End Sub

    ''' <summary>
    ''' 初期処理
    ''' </summary>
    Private Sub subInit()
        Me.optDIV1.Checked = True
        Me.optDayMon1.Checked = True
        Me.optPurchase1.Checked = True
        Me.txtEmpCode.Text = vbNullString
        Me.txtKanaName.Text = vbNullString
        Me.nupYear.Value = DateTime.Now.Year
        Me.nupMont.Value = DateTime.Now.Month
        Me.nupYearFrom.Value = DateTime.Now.Year
        Me.nupMontFrom.Value = DateTime.Now.Month
        Me.lblRowCount.Text = 0

        If DateTime.Now.Month = "12" Then
            Me.nupYearTo.Value = DateTime.Now.Year + 1
            Me.nupMontTo.Value = "1"
        End If

        Me.dgvList.Rows.Clear()
    End Sub

    ''' <summary>
    ''' 個別/範囲指定の切替
    ''' </summary>
    Private Sub subDivide()

        Try
            Me.dgvList.Rows.Clear()

            If Me.optDIV1.Checked = True Then
                Me.txtEmpCode.Enabled = True
                Me.txtKanaName.Enabled = True

                Me.dgvList.Columns("COL_USER_ID").Visible = True
                Me.dgvList.Columns("COL_USER_NAME").Visible = True
                Me.dgvList.Columns("COL_USER_KANA").Visible = True

                Me.dgvList.Columns("COL_GROUP_CD").Visible = True
                Me.dgvList.Columns("COL_SECTION_CD").Visible = True
                Me.dgvList.Columns("COL_MENU_A").Visible = False
                Me.dgvList.Columns("COL_MENU_B").Visible = False
            Else
                Me.dgvList.Columns("COL_USER_ID").Visible = False
                Me.dgvList.Columns("COL_USER_NAME").Visible = False
                Me.dgvList.Columns("COL_USER_KANA").Visible = False

                Me.dgvList.Columns("COL_GROUP_CD").Visible = False
                Me.dgvList.Columns("COL_SECTION_CD").Visible = False
                Me.dgvList.Columns("COL_MENU_A").Visible = True
                Me.dgvList.Columns("COL_MENU_B").Visible = True

                Me.txtEmpCode.Enabled = False
                Me.txtKanaName.Enabled = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' CSV保存
    ''' </summary>
    Public Sub SaveToCsv()

        '1行もデータが無い場合は、保存を中止します。
        If Me.dgvList.Rows.Count = 0 Then
            Exit Sub
        End If

        '変数を定義します。
        Dim i As Integer
        Dim j As Integer
        Dim strFileName As String
        Dim strResult As New System.Text.StringBuilder


        'コラムヘッダを1行目に列記します。
        '※ヘッダ行が不要な場合は削除可能です。
        For i = 0 To dgvList.Columns.Count - 1
            Select Case i
                Case 0
                    strResult.Append("" & dgvList.Columns(i).HeaderText.ToString & "")

                Case dgvList.Columns.Count - 1
                    If dgvList.Columns(i).Visible = True Then
                        strResult.Append("," & "" & dgvList.Columns(i).HeaderText.ToString & "" & vbCrLf)
                    Else
                        strResult.Append(vbCrLf)
                    End If
                Case Else
                    If dgvList.Columns(i).Visible = True Then
                        strResult.Append("," & "" & dgvList.Columns(i).HeaderText.ToString & "")
                    End If
            End Select

        Next

        'データを保存します。
        '※新規行の追加を認めている場合は、次行の
        '「dgvList.Columns.Count - 1」を
        '「dgvList.Columns.Count - 2」としてください。
        For i = 0 To dgvList.Rows.Count - 1
            For j = 0 To dgvList.Columns.Count - 1
                Select Case j
                    Case 0
                        strResult.Append("'" & dgvList.Rows(i).Cells(j).Value.ToString & "")

                    Case dgvList.Columns.Count - 1
                        If dgvList.Columns(j).Visible = True Then
                            'strResult.Append("," & "'" & dgvList.Rows(i).Cells(j).Value.ToString & "" & vbCrLf)
                            strResult.Append("," & dgvList.Rows(i).Cells(j).Value.ToString & "" & vbCrLf)
                        Else
                            strResult.Append(vbCrLf)
                        End If
                    Case Else
                        If dgvList.Columns(j).Visible = True Then
                            'strResult.Append("," & "'" & dgvList.Rows(i).Cells(j).Value.ToString & "")
                            strResult.Append("," & dgvList.Rows(i).Cells(j).Value.ToString & "")
                        End If
                End Select

            Next
        Next

        'ファイル名を保存ダイアログで指定した値に設定します。
        strFileName = Me.sfdCsvFile.FileName

        'Shift-JISで保存します。
        Dim swText As New System.IO.StreamWriter(strFileName,
            False, System.Text.Encoding.GetEncoding(932))

        swText.Write(strResult.ToString)
        swText.Dispose()


    End Sub

#End Region

End Class