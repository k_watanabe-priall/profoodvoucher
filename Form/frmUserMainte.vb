﻿Public Class frmUserMainte
    ''' <summary>
    ''' リストボックス再読み込み用
    ''' </summary>
    Private Sub subUserLoad()
        'TODO: このコード行はデータを 'D_prim_manurtyDataSet.EiyouUser' テーブルに読み込みます。必要に応じて移動、または削除をしてください。
        Me.EiyouUserTableAdapter.Fill(Me.D_prim_manurtyDataSet.EiyouUser)
        Me.lstUser.DataSource = Me.EiyouUserBindingSource

        Call fncGetUser(1)
    End Sub

#Region "Form Events"
    ''' <summary>
    ''' FormLoad
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub frmUserMainte_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '---初期処理
        Call subUserLoad()
    End Sub
#End Region

#Region "Sub Routine"
    ''' <summary>
    ''' ユーザ情報取得
    ''' </summary>
    ''' <param name="Proc"></param>
    ''' <returns></returns>
    Private Function fncGetUser(ByVal Proc As Integer) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim intCounter As Integer = 0
        Dim objData As New d_prim_manurtyDataSetTableAdapters.UsersTableAdapter
        Dim dt As d_prim_manurtyDataSet.UsersDataTable

        Try
            Call fncDBConnect()

            strSQL.Clear()
            strSQL.AppendLine("SELECT UserID")
            strSQL.AppendLine("FROM trnUser ")

            ds = fncAdptSQL(objConnect_LocalDB, strSQL.ToString)

            If ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                Return RET_NOTFOUND
            End If

            Erase aryFoodUser
            For intCounter = 0 To ds.Tables(0).Rows.Count - 1
                ReDim Preserve aryFoodUser(intCounter)

                With aryFoodUser(intCounter)
                    .UserID = ds.Tables(0).Rows(intCounter).Item("UserID")
                End With
            Next intCounter

            If Proc = 0 Then Return RET_NORMAL

            Me.lstFoodUser.ValueMember = "UserID"
            Me.lstFoodUser.DisplayMember = "name"
        Catch ex As Exception
            Call subOutLog(RET_ERROR, ex.Message, strMethod)

        Finally
            Call fncDBDisConnect()
        End Try
    End Function
#End Region
End Class