﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHistory
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.optPurchase2 = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEmpCode = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtKanaName = New System.Windows.Forms.TextBox()
        Me.optPurchase1 = New System.Windows.Forms.RadioButton()
        Me.dtDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtDateTo = New System.Windows.Forms.DateTimePicker()
        Me.cmdSearch = New System.Windows.Forms.Button()
        Me.cmdClear = New System.Windows.Forms.Button()
        Me.dtDate = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.COL_PURCHASE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_PURCHASE_TIME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_USER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_USER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_USER_KANA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_MENU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_PRICE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_CID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.cmdSaveCSV = New System.Windows.Forms.Button()
        Me.sfdCsvFile = New System.Windows.Forms.SaveFileDialog()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblRowCount = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1249, 139)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 19)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1243, 117)
        Me.Panel1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 10
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.03297!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.96703!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 174.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 171.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 18.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 148.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 153.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.optPurchase2, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtEmpCode, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtKanaName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.optPurchase1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dtDateFrom, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dtDateTo, 5, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdSearch, 7, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdClear, 9, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dtDate, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel4, 3, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1237, 111)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'optPurchase2
        '
        Me.optPurchase2.AutoSize = True
        Me.optPurchase2.Location = New System.Drawing.Point(529, 3)
        Me.optPurchase2.Name = "optPurchase2"
        Me.optPurchase2.Size = New System.Drawing.Size(138, 20)
        Me.optPurchase2.TabIndex = 5
        Me.optPurchase2.TabStop = True
        Me.optPurchase2.Text = "購入日範囲指定"
        Me.optPurchase2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "職員番号"
        '
        'txtEmpCode
        '
        Me.txtEmpCode.Location = New System.Drawing.Point(3, 38)
        Me.txtEmpCode.Name = "txtEmpCode"
        Me.txtEmpCode.Size = New System.Drawing.Size(142, 23)
        Me.txtEmpCode.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(151, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "カナ氏名"
        '
        'txtKanaName
        '
        Me.txtKanaName.Location = New System.Drawing.Point(151, 38)
        Me.txtKanaName.Name = "txtKanaName"
        Me.txtKanaName.Size = New System.Drawing.Size(198, 23)
        Me.txtKanaName.TabIndex = 2
        '
        'optPurchase1
        '
        Me.optPurchase1.AutoSize = True
        Me.optPurchase1.Checked = True
        Me.optPurchase1.Location = New System.Drawing.Point(355, 3)
        Me.optPurchase1.Name = "optPurchase1"
        Me.optPurchase1.Size = New System.Drawing.Size(106, 20)
        Me.optPurchase1.TabIndex = 3
        Me.optPurchase1.TabStop = True
        Me.optPurchase1.Text = "購入日指定"
        Me.optPurchase1.UseVisualStyleBackColor = True
        '
        'dtDateFrom
        '
        Me.dtDateFrom.Enabled = False
        Me.dtDateFrom.Location = New System.Drawing.Point(529, 38)
        Me.dtDateFrom.Name = "dtDateFrom"
        Me.dtDateFrom.Size = New System.Drawing.Size(165, 23)
        Me.dtDateFrom.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.Location = New System.Drawing.Point(700, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(19, 27)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "~"
        '
        'dtDateTo
        '
        Me.dtDateTo.Enabled = False
        Me.dtDateTo.Location = New System.Drawing.Point(725, 38)
        Me.dtDateTo.Name = "dtDateTo"
        Me.dtDateTo.Size = New System.Drawing.Size(164, 23)
        Me.dtDateTo.TabIndex = 8
        '
        'cmdSearch
        '
        Me.cmdSearch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearch.Location = New System.Drawing.Point(913, 38)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(142, 35)
        Me.cmdSearch.TabIndex = 9
        Me.cmdSearch.Text = "検　索"
        Me.cmdSearch.UseVisualStyleBackColor = True
        '
        'cmdClear
        '
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClear.Location = New System.Drawing.Point(1086, 38)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(128, 35)
        Me.cmdClear.TabIndex = 10
        Me.cmdClear.Text = "クリア"
        Me.cmdClear.UseVisualStyleBackColor = True
        '
        'dtDate
        '
        Me.dtDate.Location = New System.Drawing.Point(355, 38)
        Me.dtDate.Name = "dtDate"
        Me.dtDate.Size = New System.Drawing.Size(168, 23)
        Me.dtDate.TabIndex = 4
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.Panel2)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 147)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1249, 739)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvList)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 19)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1243, 717)
        Me.Panel2.TabIndex = 0
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 12.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_PURCHASE_DATE, Me.COL_PURCHASE_TIME, Me.COL_USER_ID, Me.COL_USER_NAME, Me.COL_USER_KANA, Me.COL_MENU, Me.COL_PRICE, Me.COL_ID, Me.COL_CID})
        Me.dgvList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvList.Location = New System.Drawing.Point(0, 0)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.RowTemplate.Height = 21
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(1243, 717)
        Me.dgvList.TabIndex = 0
        '
        'COL_PURCHASE_DATE
        '
        Me.COL_PURCHASE_DATE.HeaderText = "購入日"
        Me.COL_PURCHASE_DATE.Name = "COL_PURCHASE_DATE"
        Me.COL_PURCHASE_DATE.Width = 150
        '
        'COL_PURCHASE_TIME
        '
        Me.COL_PURCHASE_TIME.HeaderText = "購入時間"
        Me.COL_PURCHASE_TIME.Name = "COL_PURCHASE_TIME"
        Me.COL_PURCHASE_TIME.Width = 130
        '
        'COL_USER_ID
        '
        Me.COL_USER_ID.HeaderText = "職員番号"
        Me.COL_USER_ID.Name = "COL_USER_ID"
        Me.COL_USER_ID.Width = 150
        '
        'COL_USER_NAME
        '
        Me.COL_USER_NAME.HeaderText = "職員氏名"
        Me.COL_USER_NAME.Name = "COL_USER_NAME"
        Me.COL_USER_NAME.Width = 200
        '
        'COL_USER_KANA
        '
        Me.COL_USER_KANA.HeaderText = "カナ氏名"
        Me.COL_USER_KANA.Name = "COL_USER_KANA"
        Me.COL_USER_KANA.Width = 200
        '
        'COL_MENU
        '
        Me.COL_MENU.HeaderText = "購入メニュー"
        Me.COL_MENU.Name = "COL_MENU"
        Me.COL_MENU.Width = 130
        '
        'COL_PRICE
        '
        Me.COL_PRICE.HeaderText = "価 格"
        Me.COL_PRICE.Name = "COL_PRICE"
        '
        'COL_ID
        '
        Me.COL_ID.HeaderText = "ユーザID"
        Me.COL_ID.Name = "COL_ID"
        Me.COL_ID.Visible = False
        '
        'COL_CID
        '
        Me.COL_CID.HeaderText = "カードUID"
        Me.COL_CID.Name = "COL_CID"
        Me.COL_CID.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.Panel3)
        Me.GroupBox3.Location = New System.Drawing.Point(3, 892)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1249, 92)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.TableLayoutPanel2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 19)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1243, 70)
        Me.Panel3.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 5
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 152.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 158.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.cmdClose, 4, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cmdSaveCSV, 2, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1237, 60)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'cmdClose
        '
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClose.Location = New System.Drawing.Point(1082, 3)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(152, 54)
        Me.cmdClose.TabIndex = 1
        Me.cmdClose.Text = "閉じる"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'cmdSaveCSV
        '
        Me.cmdSaveCSV.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSaveCSV.Location = New System.Drawing.Point(901, 3)
        Me.cmdSaveCSV.Name = "cmdSaveCSV"
        Me.cmdSaveCSV.Size = New System.Drawing.Size(146, 54)
        Me.cmdSaveCSV.TabIndex = 0
        Me.cmdSaveCSV.Text = "CSV出力"
        Me.cmdSaveCSV.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.lblRowCount)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Location = New System.Drawing.Point(529, 82)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(165, 26)
        Me.Panel4.TabIndex = 11
        '
        'lblRowCount
        '
        Me.lblRowCount.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblRowCount.Location = New System.Drawing.Point(37, 7)
        Me.lblRowCount.Name = "lblRowCount"
        Me.lblRowCount.Size = New System.Drawing.Size(69, 19)
        Me.lblRowCount.TabIndex = 0
        Me.lblRowCount.Text = "9999"
        Me.lblRowCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.Location = New System.Drawing.Point(102, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(28, 19)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "件"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(1264, 985)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 12.0!)
        Me.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.Name = "frmHistory"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "購入履歴"
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents optPurchase2 As RadioButton
    Friend WithEvents Label1 As Label
    Friend WithEvents txtEmpCode As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtKanaName As TextBox
    Friend WithEvents optPurchase1 As RadioButton
    Friend WithEvents dtDateFrom As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents dtDateTo As DateTimePicker
    Friend WithEvents cmdSearch As Button
    Friend WithEvents cmdClear As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents dgvList As DataGridView
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents cmdClose As Button
    Friend WithEvents cmdSaveCSV As Button
    Friend WithEvents dtDate As DateTimePicker
    Friend WithEvents COL_PURCHASE_DATE As DataGridViewTextBoxColumn
    Friend WithEvents COL_PURCHASE_TIME As DataGridViewTextBoxColumn
    Friend WithEvents COL_USER_ID As DataGridViewTextBoxColumn
    Friend WithEvents COL_USER_NAME As DataGridViewTextBoxColumn
    Friend WithEvents COL_USER_KANA As DataGridViewTextBoxColumn
    Friend WithEvents COL_MENU As DataGridViewTextBoxColumn
    Friend WithEvents COL_PRICE As DataGridViewTextBoxColumn
    Friend WithEvents COL_ID As DataGridViewTextBoxColumn
    Friend WithEvents COL_CID As DataGridViewTextBoxColumn
    Friend WithEvents sfdCsvFile As SaveFileDialog
    Friend WithEvents Panel4 As Panel
    Friend WithEvents lblRowCount As Label
    Friend WithEvents Label4 As Label
End Class
