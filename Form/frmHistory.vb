﻿Imports System
Imports System.ComponentModel
Imports System.IO
Public Class frmHistory

#Region "FormEvents"
    ''' <summary>
    ''' FormLoad
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub frmHistory_Load(sender As Object, e As EventArgs) Handles Me.Load
        Call subInit()
    End Sub

    ''' <summary>
    ''' フォームが閉じられるとき
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub frmHistory_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        frmMenu.Visible = True
    End Sub
#End Region

#Region "ControlEvents"
    ''' <summary>
    ''' 購入日指定選択時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub optPurchase1_CheckedChanged(sender As Object, e As EventArgs) Handles optPurchase1.CheckedChanged
        Me.dtDate.Enabled = Me.optPurchase1.Checked
    End Sub

    ''' <summary>
    ''' 購入日範囲指定選択時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub optPurchase2_CheckedChanged(sender As Object, e As EventArgs) Handles optPurchase2.CheckedChanged
        Me.dtDateFrom.Enabled = Me.optPurchase2.Checked
        Me.dtDateTo.Enabled = Me.optPurchase2.Checked
    End Sub

    ''' <summary>
    ''' クリアボタン押下時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        Call subInit()
    End Sub

    ''' <summary>
    ''' 閉じるボタン押下時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
        Me.Close()

        frmMenu.Visible = True
    End Sub

    ''' <summary>
    ''' 検索ボタン押下時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        Call subGetHistoryData()

        Me.lblRowCount.Text = Me.dgvList.Rows.Count
    End Sub

#End Region

#Region "Sub Routine"
    ''' <summary>
    ''' 初期処理
    ''' </summary>
    Private Sub subInit()
        Me.txtEmpCode.Text = vbNullString
        Me.txtKanaName.Text = vbNullString
        Me.optPurchase1.Checked = True
        Me.lblRowCount.Text = "0"
        Me.dgvList.Rows.Clear()
    End Sub

    Private Sub subGetHistoryData()
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim Proc As Integer = 0
        Dim ds As New DataSet
        Dim intCounter As Integer = 0
        Dim strWork As String = vbNullString

        Try
            Call fncDBConnect()

            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("HistoryAno,")
            strSQL.AppendLine("buyCID,")
            strSQL.AppendLine("buyUserEmpCode,")
            strSQL.AppendLine("buyUserID,")
            strSQL.AppendLine("buyUserName,")
            strSQL.AppendLine("buyUserKana,")
            strSQL.AppendLine("buyMenuName,")
            strSQL.AppendLine("buyPrice,")
            strSQL.AppendLine("buyDate,")
            strSQL.AppendLine("buyTime,")
            strSQL.AppendLine("buyType")
            strSQL.AppendLine("FROM trnBuyHistory")

            If Me.txtEmpCode.Text.Trim <> "" Then
                strSQL.AppendLine("WHERE buyUserEmpCode LIKE '%" & Me.txtEmpCode.Text.Trim & "'")
                Proc = 1
            Else
                Proc = 0
            End If
            If Me.txtKanaName.Text.Trim <> "" Then
                If Proc = 0 Then
                    strSQL.AppendLine("WHERE buyUserKana LIKE '" & Me.txtKanaName.Text.Trim & "%'")
                    Proc = 2
                Else
                    strSQL.AppendLine("AND buyUserKana LIKE '" & Me.txtKanaName.Text.Trim & "%'")
                    Proc = 3
                End If
            End If

            If Proc = 0 Then
                strSQL.AppendLine("WHERE ")
            Else
                strSQL.AppendLine("AND ")
            End If

            If Me.optPurchase1.Checked = True Then
                strSQL.AppendLine("buyDate = '" & Me.dtDate.Value.ToString("yyyyMMdd") & "'")
                strSQL.AppendLine("ORDER BY buyTime DESC")
            Else
                strSQL.AppendLine("(buyDate >= '" & Me.dtDateFrom.Value.ToString("yyyyMMdd") & "'")
                strSQL.AppendLine("AND buyDate <= '" & Me.dtDateTo.Value.ToString("yyyyMMdd") & "')")
                strSQL.AppendLine("ORDER BY buyDate,buyTime")
            End If


            ds = fncAdptSQL(objConnect_LocalDB, strSQL.ToString)

            If ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                Call MsgBox("検索条件に合うデータが存在しませんでした。" & vbCrLf & "検索条件を変更して再度検索して下さい。", MsgBoxStyle.Exclamation, "所見購入履歴検索")
                Exit Sub
            End If

            dgvList.Rows.Clear()

            For intCounter = 0 To ds.Tables(0).Rows.Count - 1
                dgvList.Rows.Add()

                With dgvList.Rows(intCounter)
                    strWork = ds.Tables(0).Rows(intCounter).Item("buyDate")
                    strWork = strWork.Substring(0, 4) & "/" & strWork.Substring(4, 2) & "/" & strWork.Substring(6, 2)
                    .Cells("COL_PURCHASE_DATE").Value = strWork
                    strWork = ds.Tables(0).Rows(intCounter).Item("buyTime")
                    strWork = strWork.Substring(0, 2) & ":" & strWork.Substring(2, 2) & ":" & strWork.Substring(4, 2)
                    .Cells("COL_PURCHASE_TIME").Value = strWork
                    .Cells("COL_USER_ID").Value = ds.Tables(0).Rows(intCounter).Item("buyUserEmpCode")
                    .Cells("COL_USER_NAME").Value = ds.Tables(0).Rows(intCounter).Item("buyUserName")
                    .Cells("COL_USER_KANA").Value = ds.Tables(0).Rows(intCounter).Item("buyUserKana")
                    .Cells("COL_MENU").Value = ds.Tables(0).Rows(intCounter).Item("buyMenuName")
                    .Cells("COL_PRICE").Value = ds.Tables(0).Rows(intCounter).Item("buyPrice")
                    .Cells("COL_ID").Value = ds.Tables(0).Rows(intCounter).Item("buyUserID")
                    .Cells("COL_CID").Value = ds.Tables(0).Rows(intCounter).Item("buyCID")
                End With
            Next intCounter

        Catch ex As Exception
            Call subOutMessage(RET_ERROR, ex.Message, strMethod)
        Finally
            Call fncDBDisConnect()
        End Try
    End Sub

    ''' <summary>
    ''' CSV出力押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdSaveCSV_Click(sender As Object, e As EventArgs) Handles cmdSaveCSV.Click
        With sfdCsvFile
            Dim strFileName As String

            If optPurchase1.Checked = True Then
                strFileName = Replace(My.Settings.SAVE_CSV_SUM, "%yyyy年%mm月", dtDate.Value.ToString("yyyy年MM月dd日"))
                strFileName = Replace(strFileName, "食券購入集計表", "食券購入履歴一覧表")
            Else
                strFileName = Replace(My.Settings.SAVE_CSV_SUM_FT, "%yyyy1年%mm1月", dtDateFrom.Value.ToString("yyyy年MM月dd日"))
                strFileName = Replace(strFileName, "%yyyy2年%mm2月", dtDateTo.Value.ToString("yyyy年MM月dd日"))
                strFileName = Replace(strFileName, "食券購入集計表", "食券購入履歴一覧表")
            End If

            .DefaultExt = ".csv"
            .FileName = strFileName
            If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                Call SaveToCsv()
                Call MsgBox("「" & .FileName & "」を出力しました。", MsgBoxStyle.Information, "CSV出力")
            End If
        End With
    End Sub

    ''' <summary>
    ''' CSV保存
    ''' </summary>
    Public Sub SaveToCsv()

        '1行もデータが無い場合は、保存を中止します。
        If Me.dgvList.Rows.Count = 0 Then
            Exit Sub
        End If

        '変数を定義します。
        Dim i As Integer
        Dim j As Integer
        Dim strFileName As String
        Dim strResult As New System.Text.StringBuilder


        'コラムヘッダを1行目に列記します。
        '※ヘッダ行が不要な場合は削除可能です。
        For i = 0 To dgvList.Columns.Count - 3
            Select Case i
                Case 0
                    strResult.Append("" & dgvList.Columns(i).HeaderText.ToString & "")

                Case dgvList.Columns.Count - 3
                    strResult.Append("," & "" & dgvList.Columns(i).HeaderText.ToString & "" & vbCrLf)

                Case Else
                    strResult.Append("," & "" & dgvList.Columns(i).HeaderText.ToString & "")
            End Select

        Next

        'データを保存します。
        '※新規行の追加を認めている場合は、次行の
        '「dgvList.Columns.Count - 1」を
        '「dgvList.Columns.Count - 2」としてください。
        For i = 0 To dgvList.Rows.Count - 1
            For j = 0 To dgvList.Columns.Count - 3
                Select Case j
                    Case 0
                        strResult.Append("'" & dgvList.Rows(i).Cells(j).Value.ToString & "")

                    Case dgvList.Columns.Count - 3
                        strResult.Append("," & "'" & dgvList.Rows(i).Cells(j).Value.ToString & "" & vbCrLf)

                    Case Else
                        strResult.Append("," & "'" & dgvList.Rows(i).Cells(j).Value.ToString & "")
                End Select

            Next
        Next

        'ファイル名を保存ダイアログで指定した値に設定します。
        strFileName = Me.sfdCsvFile.FileName

        'Shift-JISで保存します。
        Dim swText As New System.IO.StreamWriter(strFileName,
            False, System.Text.Encoding.GetEncoding(932))

        swText.Write(strResult.ToString)
        swText.Dispose()


    End Sub

#End Region
End Class